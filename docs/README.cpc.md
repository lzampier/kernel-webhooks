# Commit Policy Checker

Set of simple functions for dist-git policy expression evaluation as presented
at <http://pkgs.devel.redhat.com/rules.html>.

It uses RPN (Reverse Polish Notation) aka postfix notation for evaluation and
performs some basic syntax checks, which should be enough.

Here follows some basic usage of the cpc module. Note it does not automatically
fetch the dist-git policies nor bugzilla information. It's just a simple
tool for evaluation only. The policies can be obtained from the html page.
Since the gitbzverify.py tool, which is used to generate the html page, can
also generate rules in plain text, we can probably ask rcm to generate
rules.txt along with rules.html. The cpc also comes with some basic
functionality for testing purposes only. Please see cpc.py -h for more
information.

------------------------------8<---------------------------------

```python

#!/usr/bin/env python3

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import cpc

# policy as presented at http://pkgs.devel.redhat.com/rules.html
rhel840_policy = '((release == + and zstream == + and zstream_target_release == 8.4.0) or ((jiraProject == RHELPLAN) and ((jiraField(release) == +) or (jiraField(BZ release) == +)) and ((jiraField(zstream) == +) or (jiraField(BZ zstream) == +)) and (jiraField(ZStream Target Release) == 8.4.0)))'

# 1) parse policy string and generate list of tokens
tokens = cpc.tokenize(rhel840_policy)

# 2) convert token list from infix to postfix notation
tokens = cpc.rpn(tokens)

# 3) set variables values
for token in tokens:
    # skip tokens which are not variables
    if token.type != cpc.Token.VAR:
        continue

    # set values based on the bugzilla info
    if token.name == 'release':
        token.value = '+'
    elif token.name == 'zstream':
        token.value = '-'
    elif token.name == 'zstream_target_release':
        token.value = '8.4.0'
    else:
        # all other values should be empty
        token.value = ''

# 4) evaluate the rule with 
failed = [] # will contain list of failed '==' expressions
res = cpc.evaluate(tokens, failed)
if res:
    print('Approved')
else:
    print('Unapproved: {}'.format(','.join(failed)))
    # you can also use the errexp function to get user friendly string
    # representing the failed conditions within the original expression
    # context. For example:
    # print('Unapproved: {}'.format(errexp(tokens)))

# 5) for evaluation of the same expression with different values repeat
#    steps 3) and 4).

```

------------------------------8<---------------------------------

"""

```python
if __name__ == "__main__":
    # ONLY FOR TESTING, DO NOT USE THIS AS A STANDALONE UTILITY
    import argparse
    import urllib.parse
    import urllib.request
    import json
    import os
    import sys

    def get_bugs(ids, url, apikey, fields):
        apikey = urllib.parse.quote_plus(apikey)
        fields = urllib.parse.quote_plus(fields)
        limit = 500
        todo = ids.copy()
        bugs = []
        while todo:
            l = todo[:limit]
            todo = todo[limit:]
            ids = ','.join(l)
            u = url + '/rest/bug?limit=0&api_key={}&include_fields={}&id={}'.format(apikey, fields, ids)
            req = urllib.request.Request(u)
            res = urllib.request.urlopen(req)
            data = json.loads(res.read().decode())
            if 'error' in data:
                raise Exception('Bugzilla query returned error code {}: {}'.format(data['code'], data['message']))
            bugs += data['bugs']

        return bugs

    def query_bugs(url, apikey):
        apikey = urllib.parse.quote_plus(apikey)
        # generated by pbaq https://gitlab.com/redhat/rhel/sst/kernel-maintainers/kmt/-/blob/master/kmt/pbaq.py
        # for 'classification == "Red Hat" && product == "Red Hat Enterprise Linux 8" && component == kernel && (status == ASSIGNED || status == NEW || status == POST || status == MODIFIED) && itr == 8.6.0'
        url = url + '/rest/bug?query_format=advanced&&f4=classification&o4=equals&v4=Red%20Hat&f6=product&o6=equals&v6=Red%20Hat%20Enterprise%20Linux%208&f8=component&o8=equals&v8=kernel&f13=bug_status&o13=equals&v13=ASSIGNED&f16=bug_status&o16=equals&v16=NEW&f19=bug_status&o19=equals&v19=POST&f22=bug_status&o22=equals&v22=MODIFIED&f10=OP&j10=OR&f23=CP&f25=cf_internal_target_release&o25=equals&v25=8.6.0&f2=OP&j2=AND&f26=CP&api_key={}&limit=0'.format(apikey)
        req = urllib.request.Request(url)
        res = urllib.request.urlopen(req)
        data = json.loads(res.read().decode())
        if 'error' in data:
            raise Exception('Bugzilla query returned error code {}: {}'.format(data['code'], data['message']))
        ids = [str(bug['id']) for bug in data['bugs']]
        return ids

    def get_bug_data(bug):
        data = {}
        for flag in bug['flags']:
            data[flag['name']] = flag['status']

        if 'cf_internal_target_release' in bug:
            data['internal_target_release'] = bug['cf_internal_target_release']

        if 'cf_zstream_target_release' in bug:
            data['zstream_target_release'] = bug['cf_zstream_target_release']

        return data

    def set_values(tokens, bug):
        data = get_bug_data(bug)
        for token in tokens:
            if token.type != Token.VAR:
                continue
            if token.name in data and data[token.name] != None:
                token.value = data[token.name]
            else:
                token.value = ''

    POLICY = '((release == + and internal_target_release == 8.6.0) or ((jiraProject == RHELPLAN) and ((jiraField(release) == +) or (jiraField(BZ release) == +)) and (jiraField(fixVersions) == 8.6.0)))'

    parser = argparse.ArgumentParser(description='''Dist-git bug policy checker.
    This is for cpc testing purposes only. If started with no arguments it
    queries for all 8.6.0 pending bugs(NEW, ASSIGNED, POST, MODIFIED) and verifies them
    against the rhel-8.6.0 policy, which is hardcoded in the code and may not be
    valid. Please make sure you provide the bugzilla apikey with the -a option
    or set the BUGZILLA_API_KEY env variable. Besides this default behaviour
    the dist-git policy can be specified as positional argument and bugs, which
    should be checked, can be entered via stdin. For example:
    echo 1962537 | ./cpc.py --stdin '((release == + and zstream == + and
    zstream_target_release == 8.2.0) or ((jiraProject == RHELPLAN) and
    ((jiraField(release) == +) or (jiraField(BZ release) == +)) and
    ((jiraField(zstream) == +) or (jiraField(BZ zstream) == +)) and
    (jiraField(ZStream Target Release) == 8.2.0)))'
    ''')
    parser.add_argument('policy', nargs='?', metavar='POLICY', default=POLICY,
            help='dist-git policy string as presented at "http://pkgs.devel.redhat.com/rules.html"')
    parser.add_argument('-', '--stdin', action='store_true',
            help='read bug ids which should be checked from stdin')
    parser.add_argument('-d', '--debug', action='store_true',
            help='print debug info for each evaluation step')
    parser.add_argument('-b', '--bugzilla', default='https://bugzilla.redhat.com',
            help='bugzilla url, default: "https://bugzilla.redhat.com"')
    parser.add_argument('-a', '--apikey', default=os.environ.get('BUGZILLA_API_KEY', 'None'),
            help='bugzilla apikey, default: use env variable "BUGZILLA_API_KEY"')

    args = parser.parse_args()

    ids = []
    if args.stdin:
        for bugid in sys.stdin:
            ids += bugid.split()
        ids = list(set(ids))
    else:
        print('Running default bugzilla query...')
        ids = query_bugs(args.bugzilla, args.apikey)

    bz_fields = 'id,flags,keywords,cf_internal_target_release,cf_zstream_target_release'
    print('Fetching data from bugzilla for {} bugs...'.format(len(ids)))
    bugs = get_bugs(ids, args.bugzilla, args.apikey, bz_fields)

    tokens = tokenize(args.policy)
    if args.debug:
        print('TOKENIZE' + '\n' + '-'*80 + '\n' + dump_tokens(tokens))

    tokens = rpn(tokens)
    if args.debug:
        print('RPN' + '\n' + '-'*80 + '\n' + dump_tokens(tokens))

    print('Checking bugs')

    for bug in bugs:
        bugid = bug['id']
        set_values(tokens, bug)
        if args.debug:
            print('VALUES({})'.format(bugid) + '\n' + '-'*80 + '\n' + dump_tokens(tokens))

        failed = []
        steps = []
        res = evaluate(tokens, failed, steps)
        if args.debug:
            print('EVALUATE({})'.format(bugid) + '\n' + '-'*80 + '\n' + dump_tokens(steps))

        if res:
            print('{}: Approved'.format(bugid))
        else:
            #print('{}: Unapproved: {}'.format(bugid, ','.join(failed)))
            print('{}: Unapproved: {}'.format(bugid, errexp(tokens)))
```
