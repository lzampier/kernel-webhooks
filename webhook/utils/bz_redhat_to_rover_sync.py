"""Map bugzilla redhat group to Rover LDAP group."""

import argparse
import os
import re

import bugzilla
from cki_lib import logger
from cki_lib.misc import sentry_init
import ldap
from ldapurl import LDAP_SCOPE_SUBTREE
import requests
import sentry_sdk

LOGGER = logger.get_logger('webhook.utils.bz_redhat_to_rover_sync')

# Bugzilla info
BZ_URL = 'https://bugzilla.redhat.com/xmlrpc.cgi'
BZ_GROUP = 'redhat'


def get_bz_redhat_group(args):
    """Get Bugzilla redhat group names."""
    bz_group = set()

    api_key = args.apikey
    bug = bugzilla.Bugzilla(url=BZ_URL, api_key=api_key)
    results = bug.getgroup(BZ_GROUP, membership=True)
    membership = results.membership
    for member in membership:
        bz_group.add(member['email'].lower())

    return bz_group


def get_rover_redhat_group(args):
    """Get Rover Redhat membership list."""
    rover_group = set()

    endpoint = args.rover_url + args.rover_group
    headers = {'Content-Type': 'application/json',
               'Accept': 'application/json'
               }
    cert = (args.cert_crt, args.cert_key)
    resp = requests.get(endpoint, headers=headers, cert=cert, timeout=60)
    resp.raise_for_status()
    response = resp.json()
    for member in response['members']:
        email = member['id'] + '@redhat.com'
        rover_group.add(email)

    return rover_group


def address_exists_in_ldap(args, rh_ld, email):
    """Check if the given email address exists in ldap."""
    filt = f"(mail={email})"
    return rh_ld.search_s(args.ldap_base_dn, LDAP_SCOPE_SUBTREE, filt, args.ldap_attrs.split(','))


def get_email_from_alias(args, rh_ld, rover_group, email):
    """Check if the given email address is an alias referenced in MX records."""
    filt = f"(rhatEmailAddress={email})"
    attrs = args.ldap_mx_attrs.split(',')
    results = rh_ld.search_s(args.ldap_mx_dn, LDAP_SCOPE_SUBTREE, filt, attrs)

    if not results:
        # nothing we can do, assume external contributor or ex-employee
        LOGGER.warning("BAD LDAP email (%s). Invalid user with group access?", email)
        return ''

    (__, record) = results[0]
    alias = record['sendmailMTAAliasValue'][0].decode()
    new_email = f"{str(alias)}@redhat.com"

    # some aliases already have the @redhat.com in them
    if re.search("redhat.com", alias):
        new_email = alias

    # ex-employees are redirected to managers through alias
    if re.search("usr/bin/formail", alias):
        LOGGER.info("IGNORING: EX-EMPLOYEE: %s", email)
        return ''

    if re.search("lists.corp.redhat.com", alias) or \
       re.search("listman.corp.redhat.com", alias) or \
       re.search("-bot@redhat.com", new_email) or \
       re.search("-list@redhat.com", new_email):
        LOGGER.info("IGNORING: List email: %s", alias)
        return ''

    if new_email in rover_group:
        return new_email

    # verify the translated email is valid
    if address_exists_in_ldap(args, rh_ld, new_email):
        return new_email

    # nothing we can do, assume external contributor or ex-employee
    LOGGER.warning("INVALID LDAP email (%s) from LDAP alias (%s)", new_email, email)
    return ''


def ldap_sanity_check_emails(args, rover_group, email_list):
    """Rover group takes any garbage user id, let's vet them with ldap first."""
    email_list_verified = set()

    rh_ld = ldap.initialize(args.ldap_url, trace_level=0)
    sasl_auth = ldap.sasl.sasl({}, 'GSSAPI')
    rh_ld.sasl_interactive_bind_s("", sasl_auth)

    for email in email_list:
        if re.search("bot.bugzilla.redhat.com", email) or \
           re.search("-bot@redhat.com", email) or \
           re.search("-list@redhat.com", email):
            LOGGER.info("IGNORING: list/bot email: %s", email)
            continue

        if re.search("-", email):
            LOGGER.info("IGNORING: assuming email with '-' is list/bot: %s", email)
            continue

        # filter out real emails from <email>+<extra>@redhat.com
        email = re.sub("\\+.*@", "@", email)

        if address_exists_in_ldap(args, rh_ld, email):
            # Verified!
            email_list_verified.add(email)
            continue

        base_email = get_email_from_alias(args, rh_ld, rover_group, email)

        if base_email:
            email_list_verified.add(base_email)

    return email_list_verified


def sync_bz_to_rover_group(args, bz_group, rover_group):
    """Sync bugzilla redhat group to Rover redhat group."""
    # Check which Bugzilla users are already in Rover
    email_list = [email for email in bz_group if email not in rover_group]

    email_list_verified = ldap_sanity_check_emails(args, rover_group, email_list)

    # Re-check existing emails with existing Rover group
    email_list_to_sync = [email for email in email_list_verified if email not in rover_group]

    endpoint = args.rover_url + args.rover_group + '/membersMod'
    cert = (args.cert_crt, args.cert_key)
    headers = {'Content-Type': 'application/json',
               'Accept': 'application/json'
               }
    payload = {"additions": []}

    for email in email_list_to_sync:
        username = re.sub("@.*", "", email)

        data = {"type": "user", "id": username}
        payload["additions"].append(data)

    if payload["additions"]:
        LOGGER.info("Adding to Rover: %s", email_list_to_sync)
        resp = requests.post(endpoint, json=payload, headers=headers, cert=cert, timeout=120)
        resp.raise_for_status()
    else:
        LOGGER.info("No new additions")


def get_parser_args():
    """Get command line arguments."""
    parser = argparse.ArgumentParser(description='Sync bugzilla redhat group to rover redhat group')

    # Global options
    parser.add_argument('-a', '--apikey', default=os.environ.get('BUGZILLA_API_KEY', 'None'),
                        help='bugzilla apikey, default: use env variable "BUGZILLA_API_KEY"')
    parser.add_argument('--cert-key', default=os.environ.get('CERTIFICATE_KEY'),
                        help='An optional path to an alternative certificate key.')
    parser.add_argument('--cert-crt', default=os.environ.get('CERTIFICATE_CRT'),
                        help='An optional path to an alternative certificate crt.')
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')

    # Rover config
    parser.add_argument('--rover-url', default=os.environ.get('ROVER_URL'),
                        help='URL of Rover API.')
    parser.add_argument('--rover-group', default=os.environ.get('ROVER_GROUP'),
                        help='Rover group to store users in.')
    # LDAP config
    parser.add_argument('--ldap-url', default=os.environ.get('LDAP_DN_URL'),
                        help='URL of LDAP server.')
    parser.add_argument('--ldap-base-dn', default=os.environ.get('LDAP_BASE_DN'),
                        help='Base DN for LDAP queries.')
    parser.add_argument('--ldap-mx-dn', default=os.environ.get('LDAP_MX_DN'),
                        help='MX record DN for LDAP queries.')
    parser.add_argument('--ldap-attrs', default=os.environ.get('LDAP_ATTRS'),
                        help='Base DN attributes for LDAP queries.')
    parser.add_argument('--ldap-mx-attrs', default=os.environ.get('LDAP_MX_ATTRS'),
                        help='MX DN attributes for LDAP queries.')

    return parser.parse_args()


def main():
    """Run program."""
    args = get_parser_args()
    sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)

    LOGGER.info("Gathering names from Rover redhat group")
    rover_group = get_rover_redhat_group(args)

    LOGGER.info("Gathering names from bugzilla redhat group")
    bz_group = get_bz_redhat_group(args)

    LOGGER.info("Sync bugzilla redhat group to Rover redhat group")
    sync_bz_to_rover_group(args, bz_group, rover_group)

    LOGGER.info("Done.")


if __name__ == '__main__':
    main()
